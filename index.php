<!DOCTYPE html>
<html>
<head>
  <title>Домашнее задание: Классы и объекты</title>
  <style type="text/css">
    div { 
   	  padding: 7px;
   	  padding-right: 20px; 
      border: solid 1px black;
   	  font-family: Verdana, Arial, Helvetica, sans-serif; 
   	  font-size: 13pt; 
   	  background: #E6E6FA;
    }
   	  body{
   	    background: #159445;
   	  }
  </style>
</head>
<body>
  <center><a href='admin.php'>Перейти на страницу доп. задания.</a></center>;

  <div>
  	<center><h1>Инкапсуляции<h1></center>
  	<p>
  	  <strong>Инкапсуляция</strong> - это механизм, который объединяет данные и код, манипулирующий этими данными.   Также   позволяет защитить данные и код в объекте и скрыть реализацию объекта от пользователя. При этом пользователь может взаимодействовать с объектом только через этот интерфейс и не может влиять на работу объекта, кроме как через интерфейс.
    </p>
  </div>

  <div>
    <center><h1>Плюсы и минусы объектов<h1></center>
    <strong>Плюсы</strong><br>
    <ul>
	  <li>
        Классы позволяют проводить конструирование из компонентов, обладающих простыми инструментами.
	  </li>
	  <li>
        Данные и операции вместе образуют определенную сущность, единственную, и не раскиданы по всей программе в разных ее частях.				
	  </li>
	  <li>
        Размещение кода и данных улучшает наглядность и удобство сопровождения программного обеспечения.
	  </li>
	  <li>
        Инкапсуляция информации защищает наиболее критичные данные от несанкционированного доступа.				
	  </li>
	  <li>
        Новый функционал, методы или свойства, можно добавить в одном месте в любой момент и данный функционал добавится всем объектам, не потребуется для каждого отдельного объекта добавлять функционал отдельно.
	  </li>
	  <li>
        Компоненты, которые используются многоразово, обычно содержат меньше ошибок, чем создавать для каждого отдельно. А если есть ошибка, то исправляется она в одном месте.
	  </li>
	  <li>
        Если перенести код в другую программу, он будет работать, не потребуется переписывать методы.
	  </li>
	</ul>

	<strong>Минусы</strong><br>
	  <ul>
	    <li>
		  Порождает огромные иерархии классов, что приводит к тому, что функциональность разносится по базовым и производным членам класса, и отследить логику работы становится сложнее.
		</li>
		<li>
		  Данный являются объектами, а это приводит к дополнительным расходам памяти.
		</li>
		<li>
		  Если не уметь пользоваться ООП, то может привести к  снижению производительности программ. :)
		</li>
	  </ul>
  </div>
</body>
</html>

<?php 
echo '<center><h1>Машины</h1></center>';
class Car 
{
	private $color;        //цвет
	private $capacity;     //мощность
	private $maxSpeed;     // максимальная скорость
	private $wheel;        // кол колес
	private $typeBody;     // вид кузова

	public function __construct($color, $capacity, $maxSpeed, $wheel, $typeBody)
	{
		$this->color=$color;
		$this->capacity=$capacity;
		$this->maxSpeed=$maxSpeed;
		$this->wheel=$wheel;
		$this->typeBody=$typeBody;
	}
	public function getColor()            //Вернуть цвет вызывающего объекта
	{
		return $this->color;
	}
	public function openCowl()            //Открой капот
	{
		echo "Капот открыт<br>";
	}
	public function checkSpeed($speed)  //Проверка, может ли машина двигаться по трассе
	{                                    //Ограничение передается в переменной $speed
		if($this->maxSpeed > $speed) {
			echo 'Машина предназначена для данной трассы, ее скорость '.$this->maxSpeed.'<br>';
		} else {
			echo 'Машина не предназначена для данной трассы, ее скорость '.$this->maxSpeed.'<br>';
		}
		echo 'Требуемая скорость '.$speed.'<br>';
	}
	
}

$bmw = new Car('black', 100, 300, 4, 'хэтчбек');
$audi = new Car('red', 120, 200, 4, 'седан');
echo $bmw->getColor().'<br>';
$audi->openCowl();
$bmw->checkSpeed(250);

echo '<center><h1>Телевизоры</h1></center>';
class Tv
{
	private $diagonal;        //Диагональ
	private $format_3D;       //Поддерживает ли 3D формат
	private $price;           //Цена
	private $typeCase;        //Вид корпуса

	public function __construct($diagonal, $format_3D, $price, $typeCase)
	{
		$this->diagonal=$diagonal;
		$this->format_3D=$format_3D;
		$this->price=$price;
		$this->$typeCase=$typeCase;
	}
	public function check_3D()               //Проверка поддерживает ли 3D
	{
		if($this->format_3D) {
			echo 'Данная модель поддерживает 3D формат.<br>';
		} else {
			echo 'Данная модель не поддерживает 3D формат.<br>';
		}
	}
}

$samsung = new Tv(19, true, 10000, 'пластик');
$lg = new Tv(17, false, 5000, 'дерево');
$samsung->check_3D();
$lg->check_3D();

echo '<center><h1>Шариковые ручки</h1></center>';
class BallPen
{
	private $typeCore;           //Вид стержня
	private $colorInk;            //Цвет пасты
	private $button;             //Кнопочная или нет
	private $price;              //Цена

	public function __construct($typeCore, $colorInk, $button, $price)
	{
		$this->typeCore=$typeCore;
		$this->colorInk=$colorInk;
		$this->button=$button;
		$this->price=$price;
	}
	public function setPrice($price)             //Установка цены
	{
		$this->price=$price;
	}
	public function getPrice()                   //Вернуть цену
	{ 
		return $this->price;
	}

}

$penBlack = new BallPen('тонкий', 'черный', true, 100);
$penBlue = new BallPen('толстый', 'синий', false, 50);
echo $penBlack->getPrice().'<br>';
$penBlack->setPrice(150);
echo $penBlack->getPrice().'<br>';
echo $penBlue->getPrice().'<br>';

echo '<center><h1>Утки</h1></center>';
class Duck
{
	const MAXAGE = 10;                           //Максимальный возраст
	private static $quantityDuck = 0;            //Количество уток
	private $name;             					 //Имя
	private $gender;            				 //Пол
	private $colorBeak;        					 //Цвет клюва
	private $paws;             					 //Количество лап
	private $age;              					 //Возраст

	public function __construct($name, $gender, $colorBeak, $paws, $age)
	{
		$this->name=$name;
		$this->gender=$gender;
		$this->colorBeak=$colorBeak;
		$this->paws=$paws;
		$this->age=$age;
		self::$quantityDuck++;
	}
	public function checkDuck()                 //Проверка возраста утки
	{
		if($this->age <= self::MAXAGE) {
			echo "Утка молодая<br>";
		} else {
			echo "Утка старая<br>";
		}
	}
	public static function getQuantityDuck()
	{
		echo self::$quantityDuck.'<br>';
	}
}

$duck_1 = new Duck('First', 'мужской', 'красный', 2, 9);
$duck_2 = new Duck('Second', 'женский', 'желтый', 2, 10);
$duck_3 = new Duck('Third', 'мужской', 'белый', 2, 11);
$duck_1->checkDuck();
$duck_2->checkDuck();
$duck_3->checkDuck();
echo $duck_1::getQuantityDuck();
echo $duck_2::getQuantityDuck();
echo $duck_3::getQuantityDuck();

//---------------------------------------------------------------------------------------------
echo '<center><h1>Товары</h1></center>';
class Product
{
	private $name;
	private $price;
	private $category;
	function __construct($name, $price, $category)
	{
		$this->name=$name;
		$this->price=$price;
		$this->category=$category;
	}
	function setPrice($price)                        //Установка цены
	{
		$this->price=$price;
	}
	function getPrice()                              //Вывод наименования и цена
	{
		echo $this->name." стоит ". $this->price.'<br>';
	}
	function checkCategory($category)
	{
		if($this->category == $category) {
			echo $this->name.' принадлежит категории '.$category.'<br>';
		} else {
			echo $this->name.' не принадлежит категории '.$category.'<br>';
		}
	}
}

$apple = new Product('Яблоко', 20, 'Фрукты');
$banana = new Product('Банан', 40, 'Фрукты');
$tomato = new Product('Помидор', 50, 'Овощ');
$apple->getPrice();
$apple->setPrice(100);
$apple->getPrice();
$apple->checkCategory('Фрукты');
$banana->checkCategory('Овощ');
$tomato->checkCategory('Овощ');
?>