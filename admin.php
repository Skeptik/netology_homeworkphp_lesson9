<?php 
require_once __DIR__.'/news.php';

class News
{
	private static $quantityArticles = 0;
	private $numberArticle;      //Номер статьи
	private $heading;            //Заголовок
	private $arcticle=[];        //Статья
	
	public function __construct($numberArticle, $heading, $arcticle)
	{
		$this->numberArticle=$numberArticle;
		$this->heading=$heading;
		$this->arcticle[]=$arcticle;
		self::$quantityArticles++;
	} 
	public static function quantityArticles() 
	{
		echo self::$quantityArticles;
	}
	public function getNumberArticle()
	{
		return $this->numberArticle;
	}
	public function getHeading()
	{
		return $this->heading;
	}
	public function getArticle()
	{
		return $this->arcticle[0];
	}
	public function getComments($obj)
	{
		echo "<span align='center'><em>".$obj->getComment()."</em></span>";
	}
}

class Comment
{
	private $comment;

	public function __construct($comment)
	{
		$this->comment=$comment;
	}
	public function getComment()
	{
		return $this->comment;
	}
}

$news = [];
$arrComment = [];
$news_1 = new News(1, "Житель Флориды спас аллигатора с помощью швабры и скотча", $arrNews[0][0]);
$news_2 = new News(2, "В небе над Германией спортивный самолет столкнулся с вертолетом", $arrNews[1][0]);
$news_3 = new News(3, "Фильм Звягинцева Нелюбовь номинировали на Оскар", $arrNews[2][0]);
array_push($news, $news_1);
array_push($news, $news_2);
array_push($news, $news_3);
$comm_1 = new Comment('Отличная статья');
$comm_2 = new Comment('Ставлю 10');
$comm_3 = new Comment('Не понравилась');
array_push($arrComment, $comm_1);
array_push($arrComment, $comm_2);
array_push($arrComment, $comm_3);
?>

<!DOCTYPE html>
<html>
<head>
  <title>Новости</title>
  <style type="text/css">
    div { 
   	  padding: 7px;
      padding-right: 20px; 
      border: solid 1px black;
   	  font-family: Verdana, Arial, Helvetica, sans-serif; 
   	  font-size: 13pt; 
   	  background: #E6E6FA;
   	}

   	body{
   	  background: #159445;
   	}

   	span{
      border: solid 1px black;
      font-size: 12pt; 
      background: #A118FF;
   	}
  </style>
</head>
<body>
  <center><a href='index.php'>Перейти назад на страницу.</a></center>
  <h4>Всего статей <?= News::quantityArticles() ?></h4>
  
  <?php
    for ($i = 0; $i < count($news); $i++) {
  ?>
  <center><h3>№ <?= $news[$i]->getNumberArticle().' '.$news[$i]->getHeading() ?></h3></center>
  <div>
    <p><?= $news[$i]->getArticle() ?></p>
  </div>
  <div>
    <strong>Комментарий:</strong><br>
	<?php $news[$i]->getComments($arrComment[$i]) ?>
  </div>
  <?php
    }
  ?>
</body>
</html>